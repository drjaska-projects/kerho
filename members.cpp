#include "members.h"



// ==================
//  public functions
// ==================

Members::Members()
{
	//this->memberCount = 0;

	vector<Member*> members;
	this->members = members;
}

Members::~Members()
{
	int loopCount = this->members.size();
	for (int i = 0;i<loopCount;i++)
	{
		delete this->members[i];
	}
}

Member * Members::addMemberToMemberlist(string name, string address)
{
			// ID equals to index, it starts from 0, measured from list size
	Member *member = new Member((this->members.size()), name, address);

	this->members.push_back(member);

	return member;
}

Member * Members::getMemberFromIndex(int index)
{
	if (index < this->members.size())
		return this->members[index];
	else
		return NULL;
}

/* // main only usable for debugging Members and Member
#include <iostream>
using std::cout;
using std::endl;
int main(void)
{
	Members memberinos;

	Member *member1 = memberinos.addMemberToMemberlist("Kesähessu", "Kurjala");
	Member *member2 = memberinos.addMemberToMemberlist("Steve", "Christmas Island");

	int member1index = member1->id;
	Member *member1FromIndex = memberinos.getMemberFromIndex(member1index);

	cout << "My name is " << member1FromIndex->name <<
	" and I live in " << member1FromIndex->address <<
	 "." << endl << endl;

	for (int i = 1;i<=100;i++)
	{
		cout << "Number " << i << " returns " <<
		member1FromIndex->doFavoriteNumericFunction(i) <<
		" after being processed." << endl;

		if (member1FromIndex->guessFavoriteNumber(i))
			cout << "Number " << i << " was right!" <<
			" ----------------------------" << endl;
		else
			cout << "Number " << i << " was wrong." << endl;
	}

	cout << endl << "My name is " << member2->name <<
	" and I live in " << member2->address << "." << endl << endl;

	for (int i = 1;i<=100;i++)
	{
		cout << "Number " << i << " returns " <<
		member2->doFavoriteNumericFunction(i) <<
		" after being processed." << endl;

		if (member2->guessFavoriteNumber(i))
			cout << "Number " << i << " was right!" <<
			" ----------------------------" << endl;
		else
			cout << "Number " << i << " was wrong." << endl;
	}

	//delete &memberinos;

	return 0;
}
*/

