#include <string>
using std::string;

#ifndef MEMBER_H
#define MEMBER_H

class Member {
private:
	int favoriteNumber; // member's favorite number, 1 to 100
	int guessCount; // counter for amount of favorite number guesses
	void assignFavoriteNumericFunction(); // set favorite function randomly
	
	// pointer to a function which takes two ints and returns an int
	int (*favoriteNumericFunction)(int favoriteNumber, int number);

public:
	int id; // member's member ID
	string name; // member's name
	string address; // member's address

	Member(int id, string name, string address);
	~Member();

	/* 
	  ask a member to do their favorite numeric function
	  to their favorite number with a given number
	  returns the resulting integer
	*/
	int doFavoriteNumericFunction(int number);

	/*
	  try to guess their favorite number
	  param guess: a number which to guess with
	  returns true/false
	*/
	bool guessFavoriteNumber(int guess);

	/*
	  ask how many times a member's favorite
	  number has been tried to guess
	*/
	int getGuessCount() { return this->guessCount; }
};

/*
// favorite numeric function possibilities
int sum(int favoriteNumber, int number);
int substract(int favoriteNumber, int number);
int multiply(int favoriteNumber, int number);
int divide(int favoriteNumber, int number);
int modulo(int favoriteNumber, int number);
*/

#endif

