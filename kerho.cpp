#include "members.h"

#include <iostream>
#include <iomanip>
#include <fstream>
using std::cout;
using std::endl;
using std::cin;
using std::ifstream;
using std::setw;
using std::setfill;



class Kerho {
private:
	Members* members;

	int inputReadState;

	int inputHandlerMenu();
	int inputHandlerAddMember();
	int inputHandlerGuessNumber();
	int inputHandlerInvestigateNumber();

public:
	Kerho();
	~Kerho();

	void printMemberList();
	void printInputInstructions(int state);
	void printFavoriteFunctionList();
	int selectInputHandler();

	int getInputReadState() { return this->inputReadState; }
};



Kerho::Kerho()
{
	this->inputReadState = 0;

	this->members = new Members();
}

Kerho::~Kerho()
{
	delete this->members;
}

void Kerho::printMemberList()
{
	int memberCount = this->members->getMemberCount();
	if (memberCount == 0)
	{
		cout << endl << "Please add a member or several before selecting this" << endl;
		return;
	}

	string prefix;
	if (memberCount < 10)
		prefix = "   ";
	else if (memberCount < 100)
		prefix = "  ";
	else
		prefix = " ";
	// no case for >1000 members, surely no one will do that...

	int longestNameLength = 0;
	int tempNameLength;

	// find the longest name of a member
	for (int i = 0;i<memberCount;i++)
	{
		tempNameLength = this->members->getMemberFromIndex(i)->name.length();

		if (tempNameLength > longestNameLength)
			longestNameLength = tempNameLength;
	}

	// print a nice table of all the members
	cout << "Here are all of our members:" << endl
	     << prefix << setw(4-prefix.length()) << setfill('0')
	     << 0 << " Return to main menu." << endl;

	// numbers are in a 4 wide column with spaces before the numbers
	// setw(4-prefix.length()) is used to calculate how many
	// leading zeroes are needed before the number

	for (int i = 0;i<memberCount;i++)
	{
		Member *member = this->members->getMemberFromIndex(i);
		cout << prefix << setw(4-prefix.length()) << setfill('0')
		     << i+1 << " Name: " << member->name;
		tempNameLength = member->name.length();
		for (int j = tempNameLength;j<longestNameLength;j++)
		{
			// add extra spaces so that all mentions
			// of "Address:" are on the same level
			// regardless of name lengths
			cout << " ";
		}
		cout << " Address: " << member->address << endl;
	}
	return;
}

void Kerho::printInputInstructions(int state)
{
	switch (state)
	{
		
		case 0:
		default:
			cout << endl << "Enjoy your stay! " << "We currently have "
			     << this->members->getMemberCount() << " members." << endl
			     << "Would you like to ..." << endl
			     << "1 (A)dd a new member to the club?" << endl
			     << "2 (G)uess the favorite number of an existing member?" << endl
			     << "3 Try to (S)uss out the favorite number of a member?" << endl
			     << "0 (E)xit our club? This (Q)uits the program." << endl << endl
			     << "enter input:" << endl;
			break;

		case 1:
			cout << endl << "Please, tell us about our newest member!" << endl;
			break;

		case 2:
			cout << endl << "Which member would you like to interact with?" << endl;
			break;

		case 3:
			cout << endl << "Which member would you like to interact with?" << endl;
			break;
	}
	return;
}

void Kerho::printFavoriteFunctionList()
{
	cout << endl
	     << "Club members have a secret favorite number between 1 and 100."
	     << endl << "They also have their own favorite numeric function."
	     << endl << "Here are all of them listed:" << endl
	     << "sum of integers                  (Favorite Number + Given Number)" << endl
	     << "substract an integer             (Favorite Number - Given Number)" << endl
	     << "multiply integers                (Favorite Number * Given Number)" << endl
	     << "modulus from an integer          (Favorite Number % Given Number)" << endl
	     << "floored division with an integer (Favorite Number / Given Number)" << endl
	     << endl;
}

int Kerho::inputHandlerMenu()
{
	char character;

	cin >> character;

	switch (character)
	{
		case '1':
		case 'A':
		case 'a':
			this->inputReadState = 1;
			return 1;
			break;

		case '2':
		case 'G':
		case 'g':
			this->inputReadState = 2;
			return 2;
			break;

		case '3':
		case 'S':
		case 's':
			this->inputReadState = 3;
			return 3;
			break;

		default:
			cout << endl << endl
			<< "!!!!! Invalid input recognized, please try again. !!!!!"
			<< endl << endl << endl;
			return -1;
			break;

		case '0':
		case 'E':
		case 'e':
		case 'Q':
		case 'q':
			cout << endl
			<< "We hope you enjoyed your time, See you soon!"
			<< endl << endl;
			return 0;
			break;
	}
}

int Kerho::inputHandlerAddMember()
{
	string firstName;
	string lastName;
	string wholeName;
	string address;

	cout << "What is their first name?" << endl
	     << "enter first name:" << endl;

	cin >> firstName;

	cout << endl << firstName << "!" << endl
	     << "What is their last name?" << endl
	     << "enter last name:" << endl;

	cin >> lastName;

	cout << endl << lastName << "!" << endl
	     << "What is their home street?" << endl
	     << "enter street name:" << endl;

	cin >> address;

	cout << endl << address << "!" << endl << endl;

	wholeName = firstName + " " + lastName;

	cout << "New member added to the database:" << endl
	     << "Name: " << firstName << " " << lastName
	     << endl << "Address: " << address << endl
	     << "You can now try guessing their favorite number!"
	     << endl << endl;

	this->members->addMemberToMemberlist(wholeName, address);
	this->inputReadState = 0;

	return 1;
}

int Kerho::inputHandlerGuessNumber()
{
	int memberCount = this->members->getMemberCount();
	this->printMemberList();
	if (memberCount == 0)
	{
		this->inputReadState = 0;
		return -1;
	}

	string memberNumberInput;
	int selectedMemberNumber = -1;
	string investigationNumberInput;
	int givenNumber = 0;

	// escape the loop when we are given a valid member ID
	while (selectedMemberNumber < 1 || selectedMemberNumber > memberCount)
	{
		cout << "Who do you want to talk to?" << endl
		     << "enter a member's number:" << endl;

		cin >> memberNumberInput;

		sscanf(memberNumberInput.c_str(), "%d", &selectedMemberNumber);

		if (selectedMemberNumber == 0)
		{
			this->inputReadState = 0;
			return -1;
		}
	}

	Member *member = this->members->getMemberFromIndex(selectedMemberNumber-1);

	printFavoriteFunctionList();

	// escape the loop when given number is between 1 and 100
	while (givenNumber < 1 || givenNumber > 100)
	{
		cout << "What number would you like to guess their favorite number with?"
		     << endl << "input 0 to exit or enter a number between 1 and 100:" << endl;

		cin >> investigationNumberInput;

		sscanf(investigationNumberInput.c_str(), "%d", &givenNumber);

		if (givenNumber == 0)
		{
			this->inputReadState = 0;
			return -1;
		}
	}

	int numberOfGuesses = 1 + member->getGuessCount();

	cout << endl << "You asked " << member->name
	     << " if their favorite number was " << givenNumber << "." << endl
	     << "They said: " << endl;
	if (member->guessFavoriteNumber(givenNumber))
	{
		cout << "Yes, you guessed my favorite number with "
		     << numberOfGuesses << " guesses!" << endl << endl;
	}
	else
	{
		cout << "No, " << givenNumber << " is not my favorite number."
		     << endl << endl;
	}

	this->inputReadState = 0;
	return 2;
}

int Kerho::inputHandlerInvestigateNumber()
{
	int memberCount = this->members->getMemberCount();
	this->printMemberList();
	if (memberCount == 0)
	{
		this->inputReadState = 0;
		return -1;
	}

	string memberNumberInput;
	int selectedMemberNumber = -1;
	string investigationNumberInput;
	int givenNumber = 0;

	// escape the loop when we are given a valid member ID
	while (selectedMemberNumber < 1 || selectedMemberNumber > memberCount)
	{
		cout << "Who do you want to talk to?" << endl
		     << "enter a member's number:" << endl;

		cin >> memberNumberInput;

		sscanf(memberNumberInput.c_str(), "%d", &selectedMemberNumber);

		if (selectedMemberNumber == 0)
		{
			this->inputReadState = 0;
			return -1;
		}
	}

	Member *member = this->members->getMemberFromIndex(selectedMemberNumber-1);

	printFavoriteFunctionList();

	// escape the loop when given number is between 1 and 100
	while (givenNumber < 1 || givenNumber > 100)
	{
		cout << "What number would you like to estimate their favorite number with?"
		     << endl << "input 0 to exit or enter a number between 1 and 100:" << endl;

		cin >> investigationNumberInput;

		sscanf(investigationNumberInput.c_str(), "%d", &givenNumber);

		if (givenNumber == 0)
		{
			this->inputReadState = 0;
			return -1;
		}
	}

	cout << endl << "You asked " << member->name
	     << " to do their favorite numeric function on their" << endl 
	     << "favorite number with " << givenNumber << " and they gave you back "
	     << member->doFavoriteNumericFunction(givenNumber) << "."
	     << endl << endl;

	this->inputReadState = 0;
	return 3;
}

int Kerho::selectInputHandler()
{
	switch (this->getInputReadState())
	{
		case 0:
		default:
			return inputHandlerMenu();

		case 1:
			return inputHandlerAddMember();

		case 2:
			return inputHandlerGuessNumber();

		case 3:
			return inputHandlerInvestigateNumber();
	}
}

int main(void)
{
	Kerho kerho;

	cout << "Welcome to our club!" << endl;

	kerho.printInputInstructions(kerho.getInputReadState());

	while (kerho.selectInputHandler() != 0)
	{
		kerho.printInputInstructions(kerho.getInputReadState());
	}

	return 0;
}
