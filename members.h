#include "member.h"

// Members is a vector containing 'Member's
#include <vector>
using std::vector;

#ifndef MEMBERS_H
#define MEMBERS_H

class Members {
private:
	vector<Member*> members;

public:
	Members();
	~Members();

	/*
	  add a member to current members
	  returns a pointer to the new member
	*/
	Member * addMemberToMemberlist(string name, string address);

	/*
	  param index: index in which the fetched member is stored in
	  returns a member pointer from the given index
	*/
	Member * getMemberFromIndex(int index);

	int getMemberCount() { return this->members.size(); }
};

#endif

