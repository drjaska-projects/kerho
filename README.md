## What is Kerho?

Kerho is a small Command Line Interface program I created in C++ as a free
time project reminiscent of my 3rd programming project in University.

The project is written for the most part with procedural code
but it has object orientated programming encapsulation
for the objects' "secret" information and for data fields
which aren't supposed to be editable by others.

Kerho is a fairly minimalistic database which stores members. Each member has
a first name, a last name, a street name as an address,
a secret favorite numeric function and a random favorite number which they
keep as a secret but the user can try to estimate and guess them individually.
Members are stored in a vector which acts as a memberlist.
Members and the memberlist are managed by Kerho which is also responsible for
handling Standard Input/Ouput Streams and program's overall state.

Trivia: kerho is a finnish word for club.

## How do I use Kerho?

#### Requirements:

- clone the compiled Linux program or source file to your own device
- to (re)compile Kerho an up to date C++ compiler like g++ from gcc is needed
- Kerho is a CLI program dependant on standard input and output therefore
  it needs to be executed from the command line.

#### Recompiling Kerho:

```
$ cd /path/to/kerho/
$ g++ kerho.cpp members.cpp member.cpp -o kerho.out
```

#### Opening Kerho in a Linux terminal shell:

```
$ /path/to/kerho.out
```

Kerho will then print further instructions and wait for input until it's
closed with input or externally told to close.

## Usage once opened:

Kerho will accept numbers which are integers as input to select from options.
Letters of any case which match the first letter of words with parenthesis
around the first letter are also accepted. For example `0 (E)xit` option can
be chosen with input such as `0`, `e`, `E`, `exit` or `Eastern`.

#### Tree of operations:

- 1 Add a new member to Kerho
  - give a first name
    - give a last name
      - give a street name


- 2 Guess a member's favorite number
  - 0 cancel
  - 1...n select which member to talk to
    - 0 cancel
    - 1...100 input the number between 1 and 100 to guess with


- 3 Estimate a member's favorite number
  - 0 cancel
  - 1...n select which member to talk to
    - 0 cancel
    - 1...100 input the number between 1 and 100 to estimate with


- 0 Exit

#### Possible secret numeric functions:

```
sum of integers                  (Favorite Number + Given Number)
substract an integer             (Favorite Number - Given Number)
multiply integers                (Favorite Number * Given Number)
modulus from an integer          (Favorite Number % Given Number)
floored division with an integer (Favorite Number / Given Number)
```

#### Advanced usage:

Only a single word is parsed and interpreted per operation 
until valid argument is found. This makes it possible
to chain multiple commands with a single prompt write.

For example starting the program, entering main menu and inserting

`1 Steve Stevenson SteveStreet 1 Mark Markson MarkStreet 3 2 50 3 1 50`

as input would get parsed like this:

1 Add a new member

    input left: Steve Stevenson SteveStreet 1 Mark Markson MarkStreet 3 2 50 3 1 50

First name: Steve

    input left: Stevenson SteveStreet 1 Mark Markson MarkStreet 3 2 50 3 1 50

Last name: Stevenson

    input left: SteveStreet 1 Mark Markson MarkStreet 3 2 50 3 1 50

Street name: SteveStreet

Return to main menu

    input left: 1 Mark Markson MarkStreet 3 2 50 3 1 50

1 Add a new member

    input left: Mark Markson MarkStreet 3 2 50 3 1 50

First name: Mark

    input left: Markson MarkStreet 3 2 50 3 1 50

Last name: Markson

    input left: MarkStreet 3 2 50 3 1 50

Street name: MarkStreet

Return to main menu

    input left: 3 2 50 3 1 50

3 Estimate a member's favorite number

    input left: 2 50 3 1 50

Select 2nd member, Mark Markson

    input left: 50 3 1 50

Estimate with 50

Return to main menu

    input left: 3 1 50

3 Estimate a member's favorite number

    input left: 1 50

Select 1st member, Steve Steveson

    input left: 50

Estimate with 50

Return to main menu

    input left: <Standard Input Stream is empty, waiting for user input>

