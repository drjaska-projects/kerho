#include "member.h"

// imported for rand()
#include <cstdlib>
#include <ctime>

// imported for floor()
#include <cmath>
using std::floor;



// ==========================
//  private static functions
// ==========================

int sum(int favoriteNumber, int number) 
{
	return (favoriteNumber + number);
}

int substract(int favoriteNumber, int number)
{
	return (favoriteNumber - number);
}

int multiply(int favoriteNumber, int number)
{
	return (favoriteNumber * number);
}

int modulo(int favoriteNumber, int number)
{
	return (favoriteNumber % number);
}

int divide(int favoriteNumber, int number)
{
	return (int)floor(favoriteNumber / number);
}



// ===================
//  private functions
// ===================

void Member::assignFavoriteNumericFunction()
{
		// rand() has been seeded by Member constructor
	switch ((rand() % 5) + 1) // range 1 to 5
	{
		case 1:
		default:
			this->favoriteNumericFunction = &sum;
			break;
		case 2:
			this->favoriteNumericFunction = &substract;
			break;
		case 3:
			this->favoriteNumericFunction = &multiply;
			break;
		case 4:
			this->favoriteNumericFunction = &divide;
			break;
		case 5:
			this->favoriteNumericFunction = &modulo;
			break;
	}

	return;
}



// ==================
//  public functions
// ==================



Member::Member(int id, string name, string address)
{
	this->id = id;
	this->name = name;
	this->address = address;

	srand(time(NULL)); // seed random generation for rand() to be random

	assignFavoriteNumericFunction();

	this->favoriteNumber = ((rand() % 100) + 1); // range 1 to 100

	this->guessCount = 0;
}

Member::~Member()
{
}

int Member::doFavoriteNumericFunction(int number)
{
	// make sure we are guessing a number between 1 and 100
	if (number>=1 && number<=100)
	{
		return (*this->favoriteNumericFunction)(this->favoriteNumber, number);
	}
	else
	{
		return number;
	}
}

bool Member::guessFavoriteNumber(int guess)
{
	this->guessCount++;

	return guess == this->favoriteNumber;
}

/* // main only usable for debugging Member
#include <iostream>
using std::cout;
using std::endl;
int main(void)
{
	Member membu(1, "Kesähessu", "Kurjala");

	cout << "My name is " << membu.name <<
	" and I live in " << membu.address << "." << endl;

	for (int i = 1;i<=100;i++)
	{
		cout << "Number " << i << " returns " <<
		membu.doFavoriteNumericFunction(i) <<
		" after being processed." << endl;

		if (membu.guessFavoriteNumber(i))
			cout << "Number " << i << " was right!" <<
			" ----------------------------" << endl;
		else
			cout << "Number " << i << " was wrong." << endl;
	}

	return 0;
}
*/

